<?php
/**
 * @author Sergio Hernández Martínez <hm.sergio@gmail.com> 
 * Date: 13/03/2015
 * Time: 12:26
 */

namespace AppBundle\Utils;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Session;

class FileManager {

    /*
     * Para hacer este servicio reutilizable habría que definir el directorio de uploads en el config.yml
     */
    const UPLOAD_DIR = '/../web/uploads';
    const WEB_LOAD_DIR = '/uploads';

    /**
     * @var Session
     */
    private $session;

    /**
     * @var string
     */
    private $rootDir;

    function __construct(Session $session, $rootDir)
    {
        $this->session = $session;
        $this->rootDir = $rootDir;
    }


    /**
     * @param UploadedFile $file
     * @param $path
     * @param $slug
     * @return string
     * @throws \Exception
     */
    public function uploadFile(UploadedFile $file, $path, $slug){

        $absPath = $this->getRootDir().FileManager::UPLOAD_DIR.$path;
        $nombreArchivo = '/'.$slug.'-'.uniqid().'.'.$file->getClientOriginalExtension();
        if(file_exists($absPath) && !is_writable($absPath)) {
            throw new \Exception('Error de escritura en el directorio destino: '.$absPath);
        }
        try{
            $file->move($absPath, $nombreArchivo);
        }catch (\Exception $e){
                $this->session->getFlashBag()->add('danger', 'El archivo no ha podido ser creado en: '.$absPath);
        }

        return FileManager::WEB_LOAD_DIR . $path . $nombreArchivo;
    }

    /**
     * @param $archivoURL
     * @throws \Exception
     */
    public function deleteFile($archivoURL){

        $path_archivo = $this->getRootDir().FileManager::UPLOAD_DIR.'/..'.$archivoURL;
        if (file_exists($path_archivo) && !unlink($path_archivo))
        {
            $this->session->getFlashBag()->add('warning', 'El archivo reemplazado no ha podido ser eliminado: ' . $path_archivo);
        }
    }

    /**
     * @param array $archivosURL
     * @throws \Exception
     */
    public function deleteMultipleFiles(array $archivosURL){
        foreach($archivosURL as $URL){
           FileManager::deleteFile($URL);
        }
    }

    /**
     * @return string
     */
    private function getRootDir(){

        return $this->rootDir;
    }
} 