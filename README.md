# FileManager (versi�n Alpha 0.1)
Es una clase con la que gestionar el almacenamiento y borrado de archivos, subidos
por formulario en el servidor.  
Para darle nombre a los archivos se utiliza un nombre tipo **slug** y un c�digo �nico.  
##Configuraci�n  
###Dependencias  
+ `@session`. Un servicio incluido en Symfony2 con el que almacenar mensajes de estado para
que est�n disponibles para mostrar en las vistas.  
+ `%kernel.root_dir%`. <String> Directorio raiz de la aplicaci�n Symfony2.  
###Constantes  
Hay dos constantes:  
+ **UPLOAD_DIR**. Se indica un subdirectorio a partir del ra�z, donde se almacenar�n
las im�genes (normalmente ese directorio es `/../web/uploads`).  
+ **WEB_LOAD_DIR**. Se utiliza para construir la url desde la que ser� accesible el archivo.  
##Modificaciones pendientes
+ Dejar de utilizar variables de sesi�n para los mensajes de error y usar s�lo excepciones.  
+ No usar constantes para definir los directorios base, y extraerlo a un archivo de configuraci�n.  
+ Facilitar con una funci�n la subida m�ltiple de archivos.  